var vuejs = new Vue({
    name: "Vue_component",
    el: "#exchange",
    data: function () {
        return {
            showModal: false,
            cargo: [],
            cargoAmount: 0,
            load:
                {
                    raceDate: '',
                    pointFrom: '',
                    pointTo: '',
                    loadName: '',
                    weight: 0,
                },
            showLoadsUrl: route('loads.index'),
            addLoadUrl: route('api.load.store'),
        };
    },
    methods: {
        async getAllCargo() {
            let cargoResp = await axios.get(this.showLoadsUrl);
            this.cargo = cargoResp.data;
            this.cargoAmount = Object.keys(this.cargo).length;
        },
        normalDate(bigDate) {
            let showDate = new Date(bigDate);
            return showDate.getDate().toString().padStart(2, "0") + '.' + (showDate.getMonth() + 1).toString().padStart(2, "0") + '.' + showDate.getFullYear();
        },
        showModalMethod() {
            this.showModal = !this.showModal;
        },
        validateInput() {
            if (this.load.raceDate === '' || this.load.pointFrom === '' || this.load.pointTo === '' || this.load.loadName === '' || this.load.weight < 1 ) {
                alert('Missing data!');
            }
            else {
                console.log('this.load: ', this.load);
                this.addLoad();
            }
        },
        async addLoad() {
            let addResult = await axios
            (
                {
                    method: "post",
                    url: this.addLoadUrl,
                    data: this.load,
                }
            );
            console.log(addResult.data, addResult.status);
            let message = '';
            if (addResult.data === 'ok') {
                this.getAllCargo();
                message = 'Чудово! Записано.'
            }
            else {
                message = 'Щось не виходить...';
            }
            this.showModalMethod();
            alert(message);
            // .catch(error)
        },
    },
    mounted() {
        this.getAllCargo();
    },
});
