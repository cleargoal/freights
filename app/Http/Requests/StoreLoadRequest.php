<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLoadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'raceDate' => "required|date",
            "pointFrom" => "required|string",
            "pointTo" => "required|string",
            "loadName" => "required|string",
            "weight" => "required|numeric",
        ];
    }
}
