<?php

namespace App\Http\Controllers;

use App\Models\Load;
use App\Http\Requests\StoreLoadRequest;
use App\Http\Requests\UpdateLoadRequest;
use App\Services\LoadPointService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class LoadController extends Controller
{
    /** Show Home blade
     *
     * @return Factory|View|Application
     */
    protected function showExchange(): Factory|View|Application
    {
        return view('home');
    }

    /**
     * Display a listing of the resource.
     *
     * @param LoadPointService $pointService
     * @return JsonResponse
     */
    public function index(LoadPointService $pointService): JsonResponse
    {
        $loads = $pointService->loadsList();
        return response()->json($loads);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLoadRequest $request
     * @param LoadPointService $pointService
     * @return JsonResponse
     */
    public function store(StoreLoadRequest $request, LoadPointService $pointService): JsonResponse
    {
        try {
            $newLoad = $pointService->createLoad($request->input('loadName'), $request->input('weight'));
            $pointService->createLoadPoints(
                $newLoad->id,
                $request->input('raceDate'),
                $request->input('pointFrom'),
                $request->input('pointTo'));
            $result = 'ok';
        }catch (\Exception $exception) {
            $result = $exception->getMessage();
        }

        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Load $load
     * @return Response
     */
    public function show(Load $load)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Load $load
     * @return Response
     */
    public function edit(Load $load)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateLoadRequest $request
     * @param \App\Models\Load $load
     * @return Response
     */
    public function update(UpdateLoadRequest $request, Load $load)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Load $load
     * @return Response
     */
    public function destroy(Load $load)
    {
        //
    }
}
