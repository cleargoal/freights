<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Point extends Model
{
    use HasFactory;

    /**
     * @var $fillable string[]
     */
    protected $fillable = ['name', 'date', 'load_id'];

    /**
     * @var $casts string[]
     */
    protected $casts = [
        'name' => 'string',
        'date' => 'date',
        'load_id' => 'integer',
    ];

    /**
     * @var $rules array|string[]
     */
    protected array $rules = [
        'name' => 'required|string',
        'date' => 'required|date',
        'load_id' => 'required|integer',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new PointOrderScope());
    }

    /**
     * Relation
     *
     * @return BelongsTo
     */
    public function cargo(): BelongsTo
    {
        return $this->belongsTo(Load::class);
    }


}
