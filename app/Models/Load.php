<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Load extends Model
{
    use HasFactory;

    /**
     * @var $fillable string[]
     */
    protected $fillable = ['name', 'weight'];

    /**
     * @var $casts string[]
     */
    protected $casts = [
        'name' => 'string',
        'weight' => 'integer',
    ];

    /**
     * @var $rules array|string[]
     */
    protected array $rules = [
        'name' => 'required|string',
        'weight' => 'required|integer',
    ];

    public function points(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Point::class);
    }
}
