<?php

namespace App\Services;

use App\Models\Load;
use App\Models\Point;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class LoadPointService
{

    /** Show all Loads list
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|Collection
     */
    public function loadsList(): \Illuminate\Database\Eloquent\Collection|Collection|array
    {
        $loadsRaw = Load::query()->with('points')->get();

        $loadsArr = $loadsRaw->map(function ($item) {
            $row['date'] = $item->points->first() !== null ? $item->points->first()->date : false;
            if ($item->points === null) {
                $row['route'] = false;
            } elseif ($item->points->count() === 1) {
                $row['route'] = $item->points->implode('name', ' - ') . ' - ...тре додати';
            } else {
                $row['route'] = $item->points->implode('name', ' - ');
            }
            $row['name'] = $item->name;
            $row['weight'] = $item->weight;
            return $row;
        });

        return $loadsArr->sortBy('date')->values();
    }

    /**
     * Create/store new load
     *
     * @param $loadName
     * @param $weight
     * @return Load
     */
    public function createLoad($loadName, $weight): Load
    {
        $newLoad = new Load();
        $newLoad->name = $loadName;
        $newLoad->weight = intval($weight);
        $newLoad->save();

        return $newLoad;
    }

    /**
     * Create/store new points
     *
     * @param $loadId
     * @param $raceDate
     * @param $pointFrom
     * @param $pointTo
     * @return Collection
     */
    public function createLoadPoints($loadId, $raceDate, $pointFrom, $pointTo): Collection
    {
        // from
        $fromPoint = new Point();
        $fromPoint->date = Carbon::parse($raceDate);
        $fromPoint->name = $pointFrom;
        $fromPoint->load_id = $loadId;
        $fromPoint->save();

        //destination
        $toPoint = new Point();
        $toPoint->date = Carbon::parse($raceDate);
        $toPoint->name = $pointTo;
        $toPoint->load_id = $loadId;
        $toPoint->save();

        return collect([$fromPoint, $toPoint]);
    }
}
