<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(['Полтава','Суми','Черкаси','Луцьк','Одеса','Житомир','Ужгород','Херсон','Рiвне','Харкiв','Днiпро','Львiв',
                'Тернопiль','Вiнниця','Запорiжжя','Миргород', 'Кривий Рiг','Смела','Бровари','Краматорськ','Шепетовка',]),
            'date' => Carbon::now()->addDays(rand(0, 31)),
        ];
    }
}
