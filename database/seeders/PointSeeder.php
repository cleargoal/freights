<?php

namespace Database\Seeders;

use App\Models\Load;
use App\Models\Point;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loadsIds = Load::all()->pluck('id');
        $loads = $loadsIds->count();
        $this->creation($loads);

        // TODO - create methods for single point and/or missing point. Target is to have at least 2 points for all loads
        $exist = Point::withoutGlobalScopes()->select('load_id')->groupBy('load_id')->get()->pluck('load_id');
        $single = Point::withoutGlobalScopes()
            ->select('load_id')
            ->groupBy('load_id')->havingRaw('COUNT(load_id) = 1')
            ->get()->pluck('load_id');
        $missing = $loadsIds->diff($exist);
    }

    /**
     * Create points connected to Loads
     *
     * @param int $loads;
     * @return bool
     */
    protected function creation(int $loads): bool
    {
        Point::factory()
            ->count(50)
            ->state(new Sequence(
                    fn($sequence) => ['load_id' => rand(1, $loads)],
                )
            )
            ->create();
        return true;
    }

    /**
     * Create points for loads that are single and missing
     *
     * @param $single
     * @param $missing
     * @return bool
     */
    protected function addFull($single, $missing): bool
    {

        return true;
    }

    protected function addPairToSingle($single)
    {

    }

    protected function addMissing($missing)
    {

    }
}
